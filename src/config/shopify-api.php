<?php

return [
    'key' => env('SHOPIFY_APIKEY', null),
    'password' => env('SHOPIFY_APIPASSWORD', null),
    'url' => env('SHOPIFY_URL', null)
];